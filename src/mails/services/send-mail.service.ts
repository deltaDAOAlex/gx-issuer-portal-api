import { Injectable, InternalServerErrorException } from '@nestjs/common'
import * as nodemailer from 'nodemailer'
import { NodemailerMessage } from '../dto/send-mail.dto'

@Injectable()
export class SendMailService {
  private transporter: any

  constructor() {
    this.transporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT,
      secure: true,
      auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS
      }
    })
  }

  async send(message: NodemailerMessage): Promise<any> {
    try {
      const info = await this.transporter.sendMail(message)

      return info

    } catch (e) {
      console.error(e)
      throw new InternalServerErrorException()
    }
  }

  createRegistrationMailMessage(to, uuid) {
    const url = `${process.env.HOST_URL}/${uuid}`
    const content = 'Welcome to the Issuer Portal Demo. Please use this 24 hours valid link to issue certificates:'

    const message = {
      from: process.env.FROM_EMAIL,
      to: to,
      text: `${content} ${url}`,
      html: `${content} <a href="${url}" target="_blank" rel="noopener noreferrer">${url}</a>`,
      subject: 'Issuer Lab Registration Mail'
    }

    return message
  }
}
