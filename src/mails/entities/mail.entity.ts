import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class email_record {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  date_submitted: Date

  @Column()
  email_address: string

  @Column()
  enabled: boolean

  @Column()
  gitlab_account: string
}


