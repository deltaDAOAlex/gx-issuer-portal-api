import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class registration_record {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  email_hash: string

  @Column()
  token: string

  @Column()
  expires_at: Date
}
