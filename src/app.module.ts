import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { MailsModule } from './mails/mails.module'
import { TypeOrmModule } from '@nestjs/typeorm'
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [`.${process.env.NODE_ENV}.env`],
      cache: true,
      isGlobal: true
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      name: 'GitlabConnection',
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('POSTGRES_HOST'),
        port: configService.get('POSTGRES_PORT'),
        username: configService.get('POSTGRES_USER'),
        password: configService.get('POSTGRES_PASSWORD'),
        database: configService.get('POSTGRES_DB'),
        autoLoadEntities: true
      })
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      name: 'RegistrationConnection',
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('POSTGRES_REGISTRATION_HOST'),
        port: configService.get('POSTGRES_REGISTRATION_PORT'),
        username: configService.get('POSTGRES_REGISTRATION_USER'),
        password: configService.get('POSTGRES_REGISTRATION_PASSWORD'),
        database: configService.get('POSTGRES_REGISTRATION_DB'),
        autoLoadEntities: true
      })
    }),
    MailsModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule { }
